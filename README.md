# COBRA KAI

Core Genome Multi Locus Sequence Typing (cgMLST) Outbreak Analysis tool

## Contents
* [Introduction](#introduction)
* [Commands](#commands)
  * [amr](#amr)
  * [cluster](#cluster)
* [Installation](#installation)
  * [Conda Installation](#conda-installation)
* [Usage](#usage)

## Introduction

COBRA-KAI! Cgmlst OutBReaK AnalysIs

One of the core functions of the MRSN is the detection of potential nosocomial transmission/outbreak events within the
military health care system. Early detection of patient-to-patient or environment-to-patient transmission events can
enable the health care center's infection control experts to implement infection prevention measures that can reduce 
the risk of infection to additional patients.

Our role as the primary surveillance agency for MDR bacteria in the MTF network coupled with our high-throughput WGS 
capabilities give us an unprecedented view into the microbial populations present in the MHS. Our goal is to leverage 
this wellspring of data to improve patient care.

The tools included in COBRA-KAI are ones we commonly use in pursuit of that misison

## Commands

## amr

We routinely use [MIGHT](https://gitlab.com/bcorey/might-mrsn) to perform routine genomic characterization analyses 
(kraken2, AMR prediction, mlst). The __amr__ command in cobra-kai allows the user to apply filters to the amr_summary.csv
that is returned by MIGHT and also generate an iTOL dataset of gene presence/absence.

```
cobra-kai amr <output directory> <amr summary csv> [options]
```
__Positional arguments__
* output directory: path to the directory where output files are to be stored
* amr summary csv: path to the curated output of MIGHT to use as input

__Optional arguments__
* --significance-filter: resource file for use in filtering the AMR results according to high/low significance(if desired)
* --force: allows COBRA-KAI to overwrite output from previous runs if detected
* --verbosity: set to 0 to suppress output to the terminal and only write to the log file 


## cluster

One method that we routinely use for intra-species comparisons of clinical isolates is [core genome Multi Locus Sequence
Typing (cgMLST)](http://www.ridom.com/seqsphere/u/Core_Genome_MLST.html). While the SeqSphere software we employ is an
excellent tool for visualization with either Minimum Spanning (MS) or Neighbor Joining (NJ) trees, we required a more 
programmatic approach to detecting potential transmission clusters.

Our first approach was structured as follows:

1. Import new isolate assembly files into SeqSphere
2. Using the SeqSphere client, generate a comparison table of isolates of interest
3. Generate a pair-wise distance matrix for these isolates
4. Parse the distance matrix to generate an iTOL transmission dataset file
5. Use DBSCAN to identify highly related clusters of isolates for more detailed analysis
6. Import isolate metadata for isolates implicated in outbreak clusters
7. Provide a comprehensive spreadsheet to the user for directing further analysis and report generation

```
cobra-kai cluster <output directory> <distance matrix> [options]
```

__Positional arguments__
* output directory: path to the directory where output files are to be stored
* distance matrix: path to the distance matrix to use as input (can be .csv or .xlsx)

__Optional arguments__
* --isolate-metadata: resource file for use in filtering potential transmission clusters
* --cluster-tracking: path to the file containing the cluster tracking data i.e. previous cluster analysis output
* --force: allows COBRA-KAI to overwrite output from previous runs if detected
* --verbosity: set to 0 to suppress output to the terminal and only write to the log file, 2 for debugging messages

## Installation

This script was designed to be installed using conda

### Conda Installation

cobra-kai is designed to be installed using the conda package manager. To install:

```
conda install -c b.corey cobra-kai
```

### Docker Installation

In development... stay tuned!

## Usage

General
```
     .d8888b.   .d88888b.  888888b.   8888888b.         d8888      888    d8P         d8888 8888888 888 
    d88P  Y88b d88P" "Y88b 888  "88b  888   Y88b       d88888      888   d8P         d88888   888   888 
    888    888 888     888 888  .88P  888    888      d88P888      888  d8P         d88P888   888   888 
    888        888     888 8888888K.  888   d88P     d88P 888      888d88K         d88P 888   888   888 
    888        888     888 888  "Y88b 8888888P"     d88P  888      8888888b       d88P  888   888   888 
    888    888 888     888 888    888 888 T88b     d88P   888      888  Y88b     d88P   888   888   Y8P 
    Y88b  d88P Y88b. .d88P 888   d88P 888  T88b   d8888888888      888   Y88b   d8888888888   888    "  
     "Y8888P"   "Y88888P"  8888888P"  888   T88b d88P     888      888    Y88b d88P     888 8888888 888 
     
     
				Cgmlst OutBReAK AnalysIs
					Brendan Corey
						v1.0.0


usage: cobra-kai <command> <options>

COBRA KAI! Cgmlst OutBReak AnalysIs 

optional arguments:
  -h, --help  show this help message and exit

Available commands:
  
    amr       Generates AMR iTOL dataset from curated MIGHT output .csv files
    cluster   Perform automated cluster detection and generate potential
              transmission iTOL dataset
    test      run test set for cobra-kai and exit
    version   Get versions and exit

```

amr
```


     .d8888b.   .d88888b.  888888b.   8888888b.         d8888      888    d8P         d8888 8888888 888 
    d88P  Y88b d88P" "Y88b 888  "88b  888   Y88b       d88888      888   d8P         d88888   888   888 
    888    888 888     888 888  .88P  888    888      d88P888      888  d8P         d88P888   888   888 
    888        888     888 8888888K.  888   d88P     d88P 888      888d88K         d88P 888   888   888 
    888        888     888 888  "Y88b 8888888P"     d88P  888      8888888b       d88P  888   888   888 
    888    888 888     888 888    888 888 T88b     d88P   888      888  Y88b     d88P   888   888   Y8P 
    Y88b  d88P Y88b. .d88P 888   d88P 888  T88b   d8888888888      888   Y88b   d8888888888   888    "  
     "Y8888P"   "Y88888P"  8888888P"  888   T88b d88P     888      888    Y88b d88P     888 8888888 888 
     
     
				Cgmlst OutBReAK AnalysIs
					Brendan Corey
						v1.0.0


usage: cobra-kai amr [options] <output_directory> <amr_summary_file>

Converts the curated AMR output from MIGHT into an iTOL compatible
dataset. Optional filtering can be applied from a supplied filtering file

positional arguments:
  output                path to the directory where output is/will be stored
  amr_summary           path to the curated MIGHT amr summary file, formatted
                        as a .csv

optional arguments:
  -h, --help            show this help message and exit

Optional Arguments:
  --amr-metadata AMR_METADATA
                        path to the file containing metadata relating to amr
                        classification
  --force               force overwrite of existing output
  --verbosity {0,1,2}     the level of reporting done to the terminal window [1]

```

cluster
```

     .d8888b.   .d88888b.  888888b.   8888888b.         d8888      888    d8P         d8888 8888888 888 
    d88P  Y88b d88P" "Y88b 888  "88b  888   Y88b       d88888      888   d8P         d88888   888   888 
    888    888 888     888 888  .88P  888    888      d88P888      888  d8P         d88P888   888   888 
    888        888     888 8888888K.  888   d88P     d88P 888      888d88K         d88P 888   888   888 
    888        888     888 888  "Y88b 8888888P"     d88P  888      8888888b       d88P  888   888   888 
    888    888 888     888 888    888 888 T88b     d88P   888      888  Y88b     d88P   888   888   Y8P 
    Y88b  d88P Y88b. .d88P 888   d88P 888  T88b   d8888888888      888   Y88b   d8888888888   888    "  
     "Y8888P"   "Y88888P"  8888888P"  888   T88b d88P     888      888    Y88b d88P     888 8888888 888 
     
     
				Cgmlst OutBReAK AnalysIs
					Brendan Corey
						v1.0.0


usage: cobra-kai cluster [options] <output_directory> <distance_matrix>

Converts the distance matrix output of the SeqSphere Neighbor Joining
tree into the potential transmission connections iTOL dataset. Also performs
potential transmission/outbreak cluster detection/reporting

positional arguments:
  output                path to the directory where output is/will be stored
  distance_matrix       path to the input distance matrix

optional arguments:
  -h, --help            show this help message and exit

Optional Arguments:
  --isolate-metadata ISOLATE_METADATA
                        path to the file containing metadata relating to
                        isolate source
  --threshold THRESHOLD
                        maximum allelic distance between adjacent isolates
                        that constitutes a potential transmission event
                        [0.005]
  --cluster-tracking CLUSTER_TRACKING
                        path to the file containing the cluster tracking data
  --force               force overwrite of existing output
  --verbosity {0,1,2}     the level of reporting done to the terminal window [1]

```