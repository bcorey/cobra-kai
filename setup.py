#!/usr/bin/env python
# encoding: utf-8

import setuptools

with open("README.md", "r") as readme:
    long_description = readme.read()

setuptools.setup(
    name="cobra-kai",
    version="1.0.4",
    author="Brendan Corey",
    author_email="brendan.w.corey.ctr@mail.mil",
    description="Convert genomic information to iTOL datasets and perform programmatic outbreak cluster detection",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bcorey/cobra-kai",
    license="GPLv3",
    packages=setuptools.find_packages(),
    package_data={
        "cobra_kai": ["resources/*.txt",
                      "testing/test_resources/cluster_testing/*",
                      "testing/test_resources/amr_testing/*"]
    },
    entry_points={'console_scripts': ['cobra-kai=cobra_kai.run:cobra_kai']
                  },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    python_requires='>=3.5',
    zip_safe=False
)
