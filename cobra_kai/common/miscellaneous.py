#!/usr/bin/env python
# encoding: utf-8

import argparse
import subprocess
import sys
from pathlib import Path
import distutils.spawn
from cobra_kai.common import log
import cobra_kai
import cobra_kai.testing.run_tests


def parse_arguments(__author__, __version__):
    """
    capture user instructions from CLI. What options are available is defined by the passed kwarg entrypoint
    """

    ascii_art(logo='cobra kai', author=__author__, version=__version__)

    ####################################################################################################################
    #   Being parser definition
    ####################################################################################################################

    parser = argparse.ArgumentParser(prog='cobra kai',
                                     usage='cobra-kai <command> <options>',
                                     description=log.bold_yellow('COBRA KAI! Cgmlst OutBReak AnalysIs\n'),
                                     )

    subparsers = parser.add_subparsers(title='Available commands', help='', metavar='')

    ####################################################################################################################
    #   amr
    ####################################################################################################################
    subparser_amr = subparsers.add_parser('amr',
                                          help='Generates AMR iTOL dataset from curated MIGHT output .csv files',
                                          usage='cobra-kai amr [options] <output_directory> <amr_summary_file>',
                                          description=log.bold_yellow('Converts the curated AMR output from MIGHT into an iTOL '
                                                      'compatible dataset. Optional filtering can be applied from a '
                                                      'supplied filtering file'))

    subparser_amr.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_amr.add_argument('amr_summary', type=str, help='path to the curated MIGHT amr summary file, formatted as a .csv')

    optional_amr_args = subparser_amr.add_argument_group('Optional Arguments')
    optional_amr_args.add_argument('--significance-filter', type=str, help='path to the file containing metadata relating to amr gene significance')
    optional_amr_args.add_argument('--color-code', type=str, help='path to the file containing metadata relating to amr gene color coding')
    optional_amr_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_amr_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')

    subparser_amr.set_defaults(func=cobra_kai.run.amr)
    subparser_amr.set_defaults(entry_point='amr')

    ####################################################################################################################
    #   cluster
    ####################################################################################################################
    subparser_cluster = subparsers.add_parser('cluster',
                                          help='Perform automated cluster detection and generate potential transmission iTOL dataset',
                                          usage='cobra-kai cluster [options] <output_directory> <distance_matrix>',
                                          description=log.bold_yellow('Converts the distance matrix output of a cgMLST '
                                                                      'comparison table into the potential '
                                                                      'transmission connections iTOL dataset. Also '
                                                                      'performs potential transmission/outbreak '
                                                                      'cluster detection/reporting'))

    subparser_cluster.add_argument('output', type=str, help='path to the directory where output is/will be stored')
    subparser_cluster.add_argument('distance_matrix', type=str, help='path to the input distance matrix')

    optional_cluster_args = subparser_cluster.add_argument_group('Optional Arguments')
    optional_cluster_args.add_argument('--isolate-metadata', type=str, default=None, help='path to the file containing metadata relating to isolate source')
    optional_cluster_args.add_argument('--threshold', type=float, default=15, help='maximum allelic distance between adjacent isolates that constitutes a potential transmission event [15]')
    optional_cluster_args.add_argument('--cluster-tracking', type=str, help='path to the file containing the cluster tracking data i.e. previous cluster analysis output')
    optional_cluster_args.add_argument('--force', action='store_true', help='force overwrite of existing output')
    optional_cluster_args.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')

    subparser_cluster.set_defaults(func=cobra_kai.run.cluster)
    subparser_cluster.set_defaults(entry_point='cluster')

    ####################################################################################################################
    #   test
    ####################################################################################################################
    subparser_test = subparsers.add_parser('test',
                                              help='run test set for cobra-kai and exit',
                                              usage='cobra-kai test <command>',
                                              description=log.bold_yellow('run test data for the selected cobra-kai command'))

    subparser_test.add_argument('command', type=str, choices=['amr', 'cluster', 'color-coding'], help='command to test')
    subparser_test.add_argument('--verbosity', type=int, default=1, choices=[0, 1, 2], help='the level of reporting done to the terminal window [1]')
    subparser_test.set_defaults(func=cobra_kai.testing.run_tests.test_main)

    ####################################################################################################################
    #   version
    ####################################################################################################################
    subparser_version = subparsers.add_parser('version',
                                              help='Get versions and exit',
                                              usage='cobra-kai version',
                                              description='returns the version of the installed cobra-kai package')

    subparser_version.set_defaults(func=cobra_kai.common.miscellaneous.get_version)
    subparser_version.set_defaults(version=__version__)

    ####################################################################################################################
    #
    # Collect args from parser
    #
    ####################################################################################################################

    args = parser.parse_args()

    # turn on debug messaging if verbosity == 2
    if hasattr(args, 'verbosity'):
        if getattr(args, 'verbosity') == 2:
            setattr(args, 'debug', True)
        else:
            setattr(args, 'debug', False)

    # launch analysis
    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.print_help()


def args_validation(args):
    ####################################################################################################################
    #
    # Validate/scrub args
    #   1) Clean paths for all input files/directories
    #   2) Validate the output directory
    #   3) Initialize the logger
    #   4) Determine if output files already exist and if so whether they can be overwritten
    #   5) Validate that the required input files a) exist and b) have the required extensions
    #   6) Validate that the provided threshold (if given) falls within the acceptable range
    #
    ####################################################################################################################

    # 1) Clean and validate the output directory
    try:
        assert args.output
    except AssertionError:
        print(log.bold_red('ERROR: An output directory must be specified!'))
        raise
    args.output = path_clean(args.output)
    try:
        assert file_check(file_path=args.output, file_or_directory='directory')
    except AssertionError:
        print(log.bold_red('ERROR: Failed to locate the output directory at the specified path: ' + str(args.output)))
        raise

    # 2) Initialize the logger
    logger = log.Log(log_filename=args.output / ('cobra_kai_' + args.entry_point + '.log'), stdout_verbosity_level=args.verbosity)
    logger.log_section_header('Validating command line input')
    logger.log('The output directory is: ' + log.green(str(args.output)))
    logger.log('The log file for this run is: ' + log.green(str(logger.log_file_path)))

    # 3) Validate the input files w.r.t the entrypoint passed
    if args.entry_point in ['amr']:

        # Validate amr summary file
        args.amr_summary = path_clean(file_path=args.amr_summary)
        try:
            assert file_check(file_path=args.amr_summary, file_or_directory='file')
        except AssertionError:
            quit_with_error(logger, 'Failed to locate AMR summary file at specified path: ' + str(args.amr_summary))
        try:
            assert file_extension_check(file_path=args.amr_summary, file_extension=['.csv'])
        except AssertionError:
            quit_with_error(logger, 'AMR summary file has the wrong extension (must be .csv)!')

        # validate amr metadata file (if provided)
        if args.significance_filter:
            args.significance_filter = path_clean(file_path=args.significance_filter)
            try:
                assert file_check(file_path=args.significance_filter, file_or_directory='file')
            except AssertionError:
                quit_with_error(logger,
                                'Failed to locate AMR metadata file at specified path: ' + str(args.significance_filter))
            try:
                assert file_extension_check(file_path=args.significance_filter, file_extension=['.xlsx'])
            except AssertionError:
                quit_with_error(logger, 'AMR metadata file has the wrong extension (must be .csv)!')

        # validate amr color coding file (if provided)
        if args.color_code:
            args.color_code = path_clean(file_path=args.color_code)
            try:
                assert file_check(file_path=args.color_code, file_or_directory='file')
            except AssertionError:
                quit_with_error(logger,
                                'Failed to locate color coding file at specified path: ' + str(
                                    args.color_code))
            try:
                assert file_extension_check(file_path=args.color_code, file_extension=['.xlsx'])
            except AssertionError:
                quit_with_error(logger, 'AMR metadata file has the wrong extension (must be .xlsx)!')

    if args.entry_point in ['cluster']:

        # Validate distance matrix
        args.distance_matrix = path_clean(file_path=args.distance_matrix)
        try:
            assert file_check(file_path=args.distance_matrix, file_or_directory='file')
        except AssertionError:
            quit_with_error(logger,
                            'Failed to locate distance matrix file at specified path: ' + str(args.distance_matrix))
        try:
            assert file_extension_check(file_path=args.distance_matrix, file_extension= ['.csv', '.xlsx'])
        except AssertionError:
            quit_with_error(logger, 'Distance matrix file has the wrong extension (must be .csv or .xlsx)!')

        # Validate threshold (if provided)
        if not args.threshold == 15:
            if not isinstance(args.threshold, float) or args.threshold <= 0:
                quit_with_error(logger, 'threshold must be a number greater than 0')

        # Validate isolate metadata (if provided)
        if args.isolate_metadata:
            args.isolate_metadata = path_clean(file_path=args.isolate_metadata)
            try:
                assert file_check(file_path=args.isolate_metadata, file_or_directory='file')
            except AssertionError:
                quit_with_error(logger, 'Failed to locate isolate metadata file at specified path: ' + str(
                    args.isolate_metadata))
            try:
                assert file_extension_check(file_path=args.isolate_metadata, file_extension=['.csv'])
            except AssertionError:
                quit_with_error(logger, 'Isolate metadata file has the wrong extension (must be .csv)!')

        # Validate cluster tracking sheet (if provided)
        if args.cluster_tracking:
            args.cluster_tracking = path_clean(file_path=args.cluster_tracking)
            try:
                assert file_check(file_path=args.cluster_tracking, file_or_directory='file')
            except AssertionError:
                quit_with_error(logger, 'Failed to locate cluster tracking file at specified path: ' + str(
                    args.cluster_tracking))
            try:
                assert file_extension_check(file_path=args.cluster_tracking, file_extension=['.xlsx'])
            except AssertionError:
                quit_with_error(logger, 'Cluster tracking file has the wrong extension (must be .xlsx)!')

    # Validate that the output files don't already exist for this run
    if args.entry_point in ['amr']:

        # Validate AMR output file
        amr_output_file = args.output / 'amr_itol_dataset.txt'
        try:
            assert not file_check(file_path=amr_output_file, file_or_directory='file')
        except AssertionError:
            if args.force:
                logger.log('AMR output file: ' + str(
                    amr_output_file) + ' already exists, but --force was invoked so overwriting')
                subprocess.run(['rm', str(amr_output_file)])
            else:
                quit_with_error(logger, 'AMR output file: ' + str(amr_output_file) + ' already exists!')

    if args.entry_point in ['cluster']:

        # Validate clusters output files
        potential_transmission_pairs_file = args.output / 'potential_transmission_pairs.csv'
        try:
            assert not file_check(file_path=potential_transmission_pairs_file, file_or_directory='file')
        except AssertionError:
            if args.force:
                logger.log('Potential transmission pairs file: ' + str(
                    potential_transmission_pairs_file) + ' already exists, but --force was invoked so overwriting')
                subprocess.run(['rm', str(potential_transmission_pairs_file)])
            else:
                quit_with_error(logger, 'Potential transmission pairs file: ' + str(
                    potential_transmission_pairs_file) + ' already exists!')

        potential_transmission_itol_dataset_txt_file = args.output / 'potential_transmission_iTOL_dataset.txt'
        try:
            assert not file_check(file_path=potential_transmission_itol_dataset_txt_file, file_or_directory='file')
        except AssertionError:
            if args.force:
                logger.log('Potential transmission iTOL dataset file: ' + str(
                    potential_transmission_itol_dataset_txt_file) + ' already exists, but --force was invoked so overwriting')
                subprocess.run(['rm', str(potential_transmission_itol_dataset_txt_file)])
            else:
                quit_with_error(logger, 'Potential transmission iTOL dataset file: ' + str(
                    potential_transmission_itol_dataset_txt_file) + ' already exists!')

        cluster_information_file = args.output / 'cluster_information.csv'
        try:
            assert not file_check(file_path=cluster_information_file, file_or_directory='file')
        except AssertionError:
            if args.force:
                logger.log('Cluster information file: ' + str(
                    cluster_information_file) + ' already exists, but --force was invoked so overwriting')
                subprocess.run(['rm', str(cluster_information_file)])
            else:
                quit_with_error(logger, 'Cluster information file: ' + str(
                    cluster_information_file) + ' already exists!')

    return args, logger


def path_clean(file_path):
    """
    sometimes users are careless. This method will use the pathLib module to convert user input into full posix path
    objects to avoid confusion downstream
    """

    return Path(str(file_path)).expanduser().resolve()


def file_extension_check(file_path=None, file_extension=None, logger=None):
    """
    verify that the file provided has the correct extension
    """
    for extension in file_extension:
        if extension in file_path.suffixes:
            return True
    return False


def file_check(file_path=None, file_or_directory=None):
    """
    checks to see if the file associated with the individual_sequencing_sample object exists in the file system at the
    specified PATH. Returns True if yes and False if no. Also return False if the attribute does not exist
    """
    if file_or_directory == 'file':
        if file_path:
            if file_path.exists():
                return True
            else:
                return False
        else:
            return False

    if file_or_directory == "directory":
        if file_path:
            if file_path.is_dir():
                return True
            else:
                return False
        else:
            return False


def check_for_dependency(program):
    """
    check_for_dependency() will use distutils.spawn to determine if the input dependency is available on the users PATH
    returns True if the dependency is located, False otherwise
    """
    if distutils.spawn.find_executable(str(program)):
        return True
    else:
        return False


def quit_with_error(logger, message):
    """
    Displays the given message and ends the program's execution.
    """
    logger.log(log.bold_red('Error: ') + message, 0, stderr=True)
    sys.exit(1)


def query_yes_no(question, default=None):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def round_down(num, divisor):
    return num - (num % divisor)


def ascii_art(logo=None, author=None, version=None):
    """
    Print out the ascii art logo requested by the user
    :param logo: the name of the logo to be printed
    """

    if logo == 'cobra kai':
        print(log.bold_red("""

     .d8888b.   .d88888b.  888888b.   8888888b.         d8888      888    d8P         d8888 8888888 888 
    d88P  Y88b d88P" "Y88b 888  "88b  888   Y88b       d88888      888   d8P         d88888   888   888 
    888    888 888     888 888  .88P  888    888      d88P888      888  d8P         d88P888   888   888 
    888        888     888 8888888K.  888   d88P     d88P 888      888d88K         d88P 888   888   888 
    888        888     888 888  "Y88b 8888888P"     d88P  888      8888888b       d88P  888   888   888 
    888    888 888     888 888    888 888 T88b     d88P   888      888  Y88b     d88P   888   888   Y8P 
    Y88b  d88P Y88b. .d88P 888   d88P 888  T88b   d8888888888      888   Y88b   d8888888888   888    "  
     "Y8888P"   "Y88888P"  8888888P"  888   T88b d88P     888      888    Y88b d88P     888 8888888 888 
     
     """))
        print(log.bold_red("\t\t\t\tCgmlst OutBReAK AnalysIs"))
        print(log.bold_red("\t\t\t\t\t" + str(author)))
        print(log.bold_red("\t\t\t\t\t\tv" + str(version)))
        print('\n')


def get_version(args):

    print(log.bold('Installed version: ' + args.version))
