#!/usr/bin/env python
# encoding: utf-8

"""
outbreaker.py defines the entrypoints for performing outbreaker analyses
"""

__author__ = 'Brendan Corey'
__version__ = '1.0.3'

import pandas as pd
from cobra_kai.common.miscellaneous import parse_arguments, args_validation
from cobra_kai.processing.amr_methods import import_amr_data, amr_itol_dataset_writer, filter_insignificant_genes, \
    color_code_genes
from cobra_kai.processing.clustering_methods import distance_matrix_to_dataframe, dataframe_to_pairwise_distance, \
    threshold_filter, multi_index_series_to_dict, generate_potential_transmission_itol_dataset, \
    outbreak_cluster_detection, outbreak_cluster_metadata_decoration, import_existing_outbreak_cluster_information, \
    compare_new_and_old_outbreak_cluster_dataframes, filter_same_patient_clusters, rename_clusters


#######################################################################################################################
#
# cobra_kai():
#   entry point for cobra-kai package.
#
#######################################################################################################################
def cobra_kai():
    parse_arguments(__author__, __version__)


#######################################################################################################################
#
# amr():
#   runs amr pipeline
#
#######################################################################################################################
def amr(args):
    # validate input and initialize logger
    args, logger = args_validation(args)

    # convert the amr summary into a dataframe
    amr_dataframe = import_amr_data(amr_summary_csv=args.amr_summary, logger=logger, debug=args.debug)

    if args.significance_filter:
        amr_dataframe = filter_insignificant_genes(output_directory=args.output, amr_dataframe=amr_dataframe,
                                                   filter_file=args.significance_filter, logger=logger,
                                                   debug=args.debug)

    if args.color_code:
        amr_dataframe = color_code_genes(output_directory=args.output, amr_dataframe=amr_dataframe,
                                         filter_file=args.color_code, logger=logger, debug=args.debug)

    # write the amr summary information to an iTOL dataset
    amr_itol_dataset_writer(output_directory=args.output, amr_dataframe=amr_dataframe, logger=logger, debug=args.debug)


#######################################################################################################################
#
# cluster():
#   runs cluster pipeline
#
#######################################################################################################################
def cluster(args):
    # validate input and initialize logger
    args, logger = args_validation(args)

    # import the distance matrix into a Dataframe object
    distance_dataframe = distance_matrix_to_dataframe(distance_matrix_file=args.distance_matrix,
                                                      logger=logger,
                                                      debug=args.debug)

    # convert the Dataframe object into a multi-index Series object
    pairwise_series = dataframe_to_pairwise_distance(distance_dataframe=distance_dataframe,
                                                     logger=logger,
                                                     debug=args.debug)

    # filter the multi-index Series object using the user specified (or default) threshold
    filtered_pairwise_series = threshold_filter(series_object=pairwise_series,
                                                threshold=args.threshold,
                                                logger=logger,
                                                debug=args.debug)

    # convert the filtered multi-index Series object into a multi-index dictionary
    multi_index_dict = multi_index_series_to_dict(series_object=filtered_pairwise_series,
                                                  logger=logger,
                                                  debug=args.debug)

    # use the multi-index dictionary to generate the iTOL potential transmission dataset
    generate_potential_transmission_itol_dataset(potential_transmission_dict=multi_index_dict,
                                                 output_directory=args.output,
                                                 logger=logger,
                                                 debug=args.debug)

    # using the Dataframe object, use DBSCAN to detect/report possible transmission clusters
    outbreak_cluster_dataframe = outbreak_cluster_detection(distance_matrix_dataframe=distance_dataframe,
                                                            eps=args.threshold,
                                                            logger=logger,
                                                            debug=args.debug)

    # if the metadata file was provided, import it and apply it
    if args.isolate_metadata:
        decorated_outbreak_cluster_dataframe = outbreak_cluster_metadata_decoration(
            outbreak_cluster_dataframe=outbreak_cluster_dataframe,
            metadata_file=args.isolate_metadata,
            logger=logger,
            debug=args.debug)

        outbreak_cluster_dataframe = decorated_outbreak_cluster_dataframe

        # if the cluster-tracking file was provided, import it and apply it
        if args.cluster_tracking:
            existing_clusters_dataframe = import_existing_outbreak_cluster_information(
                existing_clusters_file=args.cluster_tracking,
                logger=logger,
                debug=args.debug)

            outbreak_cluster_dataframe = compare_new_and_old_outbreak_cluster_dataframes(
                new_outbreak_dataframe=outbreak_cluster_dataframe,
                existing_outbreak_dataframe=existing_clusters_dataframe,
                logger=logger,
                debug=args.debug)

    # if the cluster_tracking info was not provided, we will use the rename_clusters() method
    if not args.cluster_tracking:
        outbreak_cluster_dataframe = rename_clusters(new_outbreak_dataframe=outbreak_cluster_dataframe,
                                                     logger=logger, debug=args.debug)

    # if the metadata file was provided, use that data to filter out same-source clusters
    if args.isolate_metadata:
        filtered_outbreak_dataframe = filter_same_patient_clusters(
            outbreak_cluster_dataframe=outbreak_cluster_dataframe,
            args=args,
            logger=logger,
            debug=args.debug)

    # generate new outbreak cluster report
    outbreak_cluster_dataframe_file = args.output / 'cluster_analysis.xlsx'

    with pd.ExcelWriter(outbreak_cluster_dataframe_file, mode='w') as writer:
        if 'filtered_outbreak_dataframe' in locals():
            filtered_outbreak_dataframe.to_excel(writer, sheet_name='filtered', index=False)
        outbreak_cluster_dataframe.to_excel(writer, sheet_name='unfiltered', index=False)

