#!/usr/bin/env python
# encoding: utf-8
import shutil
from pathlib import Path
import numpy as np
import pandas as pd
import xlrd

from cobra_kai.common import log
from cobra_kai.common.miscellaneous import path_clean, file_check, file_extension_check, quit_with_error

"""
Methods involved in processing the Neighbor Joining (NJ) distance matrix output from SeqSphere.

Summary of methods:
    1)  nj_matrix_to_dataframe():
            This method imports the NJ distance matrix from a .csv file and returns a dataframe object containing the 
            data
    2)  dataframe_to_pairwise_distance():
            This method reduces a symmetrical dataframe to an upper triangle matrix to remove redundant data and then 
            uses the .stack() method to generate a multi-index series object where the value is the pairwise distance 
            between the indices. The series object is returned
    3)  threshold_filter():
            This method filters a series object using a passed threshold value. All rows where the value is less than or
            equal to the threshold are retained, and the filtered series is returned
    4)  multi_index_series_to_dict():
            Convert the matrix into a three column array (pairs of isolates with their allelic distance)
    5)  generate_potential_transmission_itol_dataset():
            Write all entries from a multi-index dictionary to output iTOL dataset file
    6)  outbreak_cluster_detection():
            Determination of clusters w/ same patient deduplication (if possible)
"""


#######################################################################################################################
#
# distance_matrix_to_dataframe():
#   Import the NJ distance matrix from SeqSphere into a pandas dataframe
#
#######################################################################################################################
def distance_matrix_to_dataframe(distance_matrix_file=None, logger=None, debug=False):

    logger.log_section_header('Converting distance matrix to pandas Dataframe object')

    try:
        assert distance_matrix_file
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg distance_matrix_file is required'))
        raise

    distance_matrix_file = path_clean(file_path=distance_matrix_file)

    try:
        assert file_check(file_path=distance_matrix_file, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red('ERROR: distance_matrix_to_dataframe() unable to locate the metadata_file: ' + str(distance_matrix_file)))
        raise

    try:
        assert file_extension_check(file_path=distance_matrix_file, file_extension=['.csv', '.xlsx'])
    except AssertionError:
        logger.log(log.bold_red('ERROR: distance_matrix_file must be either a .csv or .xlsx file'))
        raise

    if file_extension_check(file_path=distance_matrix_file, file_extension=['.csv']):
        try:
            distance_matrix_dataframe = pd.read_csv(distance_matrix_file, header=0, index_col=0)
        except pd.errors.EmptyDataError:  # Case of empty amr_summary_csv
            quit_with_error(logger, 'The distance matrix file was empty: ' + log.bold(str(distance_matrix_file)))
        except ValueError:  # Incorrectly formatted amr_summary_csv
            quit_with_error(logger, 'The distance matrix file was incorrectly formatted: ' + log.bold(str(distance_matrix_file)))

    if file_extension_check(file_path=distance_matrix_file, file_extension=['.xlsx']):
        try:
            xlsx = pd.ExcelFile(distance_matrix_file)
        except xlrd.biffh.XLRDError:
            logger.log(log.bold_red(
                'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(
                    str(distance_matrix_file))) + 'needs to be formatted as an .xlsx')
            raise
        try:
            distance_matrix_dataframe = pd.read_excel(xlsx, index_col=0, header=0)
        except xlrd.biffh.XLRDError:
            logger.log(log.bold_red(
                'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(
                    str(distance_matrix_file))) + ' is improperly formatted!')
            raise
        except ValueError:
            logger.log(log.bold_red(
                'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(
                    str(distance_matrix_file))) + ' is improperly formatted!')
            raise

    if debug:
        print(distance_matrix_dataframe.head(20))

    sorted_distance_matrix_dataframe = distance_matrix_dataframe.sort_index(axis=0).sort_index(axis=1)

    if debug:
        print(sorted_distance_matrix_dataframe.head(20))

    return sorted_distance_matrix_dataframe


#######################################################################################################################
#
# dataframe_to_pairwise_distance():
#   Convert the matrix into a three column array (pairs of isolates with their allelic distance
#       a) Create an upper triangle matrix from the distance matrix to remove redundant information
#       b) Generate multi-index Series object from the upper triangle matrix using stack()
#
#######################################################################################################################
def dataframe_to_pairwise_distance(distance_dataframe=None, logger=None, debug=False):

    logger.log_section_header('Converting Dataframe to multi-index Series object')
    logger.log(
        log.dim('   a) Create an upper triangle matrix from the distance matrix to remove redundant information'))
    logger.log(log.dim('   b) Generate multi-index Series object from the upper triangle matrix using stack()\n'))

    seqsphere_upper_tri_matrix = distance_dataframe.where(np.triu(np.ones(distance_dataframe.shape), 1).astype(bool))
    seqsphere_pairwise = seqsphere_upper_tri_matrix.stack()

    if debug:
        print(seqsphere_pairwise)

    return seqsphere_pairwise


#######################################################################################################################
#
# threshold_filter():
#   Filter a series object based on a passed threshold
#       a) Filter and drop all series entries where the pairwise distance is greater than the threshold
#       d) Convert the filtered Series object to a dictionary of form {(isolate 1, isolate2): distance}
#
#######################################################################################################################
def threshold_filter(series_object=None, threshold=None, logger=None, debug=False):
    logger.log_section_header('Applying threshold filter')
    logger.log(
        log.dim('   a) Filter and drop all series entries where the pairwise distance is greater than the threshold\n'))

    try:
        assert series_object is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg series_object is required'))
        raise

    try:
        assert threshold
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg threshold is required'))
        raise

    series_object_filtered = series_object.mask(series_object >= threshold).dropna()

    if debug:
        print(series_object_filtered)

    return series_object_filtered


#######################################################################################################################
#
# multi_index_series_to_dict():
#   Convert the matrix into a three column array (pairs of isolates with their allelic distance)
#       a) Filter and drop all series entries where the pairwise distance is greater than the threshold
#       d) Convert the filtered Series object to a dictionary of form {(isolate 1, isolate2): distance}
#
#######################################################################################################################
def multi_index_series_to_dict(series_object=None, logger=None, debug=False):
    logger.log_section_header('Converting multi-index Series object to dictionary')
    logger.log(log.dim('   a) Convert the Series object to a dictionary of form {(isolate 1, isolate2): distance}\n'))

    try:
        assert series_object is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg series_object is required'))
        raise

    series_as_dict = series_object.to_dict()

    if debug:
        logger.log('Printing result of multi_index_series_to_dict() for debugging')
        logger.log('Isolate 1\tIsolate 2\tDistance')
        for isolate_pair, distance in series_as_dict.items():
            logger.log(str(isolate_pair[0]) + '\t' + str(isolate_pair[1]) + '\t' + str(distance))

    return series_as_dict


#######################################################################################################################
#
# generate_potential_transmission_itol_dataset():
#   Write all entries from a multi-index dictionary to output
#       a) Write output to potential_transmission_pairs.csv as "isolate 1, isolate 2, allelic distance"
#       b) Check for the iTOL template file
#       c) Write potential transmission data to potential_transmission_iTOL_dataset.txt
#
#######################################################################################################################
def generate_potential_transmission_itol_dataset(potential_transmission_dict=None, output_directory=None, logger=None, debug=False):
    logger.log_section_header('Generating potential transmission dataset for iTOL')
    logger.log(
        log.dim('   a) Write output to potential_transmission_pairs.csv as "isolate 1, isolate 2, allelic distance"'))
    logger.log(log.dim('   b) Check for the iTOL template file'))
    logger.log(log.dim('   c) Write potential transmission data to potential_transmission_iTOL_dataset.txt'))

    import csv
    import shutil
    import cobra_kai

    try:
        assert potential_transmission_dict is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg potential_transimission_dict is required'))
        raise

    try:
        assert Path(output_directory).expanduser().resolve().is_dir()
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg output_directory is required'))
        raise

    potential_transmission_pairs_csv_file = output_directory / 'potential_transmission_pairs.csv'

    logger.log(
        '\nNow generating potential transmission pair file: ' + log.green(str(potential_transmission_pairs_csv_file)))

    with open(potential_transmission_pairs_csv_file, 'w') as output_csv:
        csv_writer = csv.writer(output_csv, delimiter=',')
        for isolate_pair, allelic_distance in potential_transmission_dict.items():
            csv_writer.writerow([isolate_pair[0], isolate_pair[1], allelic_distance])

    if debug:
        with open(potential_transmission_pairs_csv_file, 'r') as output_csv:
            csv_reader = csv.reader(output_csv, delimiter=',')
            for line in csv_reader:
                print(line)

    itol_template_file = Path(cobra_kai.__file__).parent / 'resources' / 'itol_connection_template.txt'

    try:
        assert itol_template_file.exists()
    except AssertionError:
        logger.log(log.bold_red('ERROR: Failed to locate template file: ' + str(itol_template_file)))
        raise

    logger.log('\nThe itol connection template file is: ' + log.green(str(itol_template_file)))

    potential_transmission_itol_dataset_txt_file = output_directory / 'potential_transmission_iTOL_dataset.txt'
    shutil.copyfile(itol_template_file, potential_transmission_itol_dataset_txt_file)

    try:
        assert potential_transmission_itol_dataset_txt_file.exists()
    except AssertionError:
        logger.log(log.bold_red('ERROR: Failed to locate template file: ' + str(potential_transmission_itol_dataset_txt_file)))
        raise

    logger.log('Now generating itol connection dataset: ' + log.green(str(potential_transmission_itol_dataset_txt_file)))

    with open(potential_transmission_itol_dataset_txt_file, 'a') as dataset_file:
        connecting_line_features = '5\trgba(255, 15, 15, 0.5)\tnormal\n'
        for isolate_pair, allelic_distance in potential_transmission_dict.items():
            dataset_file.write(
                str(isolate_pair[0]) + '\t' + str(isolate_pair[1]) + '\t' + connecting_line_features)

    if debug:  # print the dataset file to the terminal for inspection
        with open(potential_transmission_itol_dataset_txt_file, 'r') as debug_file:
            for line in debug_file.readlines():
                print(line.strip())


#######################################################################################################################
#
# outbreak_cluster_detection():
#   Determination of clusters w/ same patient deduplication
#       a) Use DBSCAN to perform cluster assignments for all isolates
#       b) Import metadata (if available)
#           i) Remove sets containing ONLY isolates from a single patient
#       c) Reconstruct distance matrices for detected clusters
#       d)
#
#######################################################################################################################
def outbreak_cluster_detection(distance_matrix_dataframe=None, eps=None, logger=None, debug=False):
    logger.log_section_header('Performing potential outbreak cluster detection')
    logger.log(log.dim('   a) Use DBSCAN to perform cluster assignments for all isolates'))
    logger.log(log.dim('   b) Import metadata (if available)'))
    logger.log(log.dim('      i) Remove sets containing ONLY isolates from a single patient'))
    logger.log(log.dim('   c) Remove sets containing ONLY isolates from a single patient'))
    logger.log(log.dim('   d) Reconstruct distance matrices for detected clusters'))

    from sklearn.cluster import DBSCAN

    try:
        assert distance_matrix_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg distance_matrix_dataframe is required'))
        raise

    logger.log('\nRunning DBSCAN with ' + log.bold_green('eps=' + str(eps)) + ' and ' + log.bold_green('min_samples=2\n'))

    # a) Use DBSCAN to perform cluster assignments for all isolates
    db = DBSCAN(eps=eps, min_samples=2, metric='precomputed').fit_predict(distance_matrix_dataframe)
    clusters = []

    if debug:
        print(db)

    for cluster in set(db):
        if cluster != -1:
            clusters.append(sorted([i[1] for i in set(zip(db, distance_matrix_dataframe.index)) if i[0] == cluster]))

    logger.log('\nNumber of clusters detected before filtering: ' + str(len(clusters)))
    for cluster in range(0, len(clusters)):
        logger.log('\n' + log.underline('Cluster ' + str(cluster+1) + ' (n=' + str(len(clusters[cluster])) + ')' + ' contains the following isolates:'))
        for isolate in clusters[cluster]:
            logger.log('   ' + str(isolate))

    # Generate a dataframe of clustered isolates with metadata
    flattened_cluster_dict = {'Sample ID': [], 'cluster': []}
    for cluster in range(0, len(clusters)):
        for isolate in clusters[cluster]:
            flattened_cluster_dict['Sample ID'].append(isolate)
            flattened_cluster_dict['cluster'].append(cluster)

    if debug:
        print(flattened_cluster_dict)

    column_list = ['Sample ID', 'cluster']

    cluster_dataframe = pd.DataFrame(flattened_cluster_dict, columns=column_list)

    if debug:
        print(cluster_dataframe)

    return cluster_dataframe


#######################################################################################################################
#
# outbreak_cluster_metadata_decoration():
#   NOTE: This method should only run if isolate metadata is available!
#
#   1) Validate passed kwargs
#   2) Import isolate metadata using metadata_importer()
#   3) Merge the isolate metadata and the cluster dataframes s.t. isolates in clusters are decorated with metadata
#      applicable for reporting/downstream operations
#   4) Return the decorated dataframe
#
#######################################################################################################################
def outbreak_cluster_metadata_decoration(outbreak_cluster_dataframe=None, metadata_file = None, logger=None, debug=False):
    logger.log_section_header('Applying isolate metadata to potential outbreak clusters')
    logger.log(log.dim('\ta)Import metadata from file --isolate-metadata using metadata_importer()'))
    logger.log(log.dim('\tb) Merge isolate metadata to outbreak clusters on Sample ID'))
    logger.log(log.dim('\tc) Returns the metadata decorated outbreak cluster Dataframe object'))

    # validate passed kwargs
    try:
        assert outbreak_cluster_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg outbreak_cluster_dataframe is required by '
                                'outbreak_cluster_metadata_decoration()'))
        raise

    try:
        assert metadata_file
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg metadata_file is required by outbreak_cluster_metadata_decoration()'))
        raise

    metadata_file = path_clean(file_path=metadata_file)

    try:
        assert file_check(file_path=metadata_file, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red('ERROR: outbreak_cluster_metadata_decoration() unable to locate the metadata_'
                                'file: ' + str(metadata_file)))
        raise

    try:
        assert file_extension_check(file_path=metadata_file, file_extension=['.csv'])
    except AssertionError:
        logger.log(log.bold_red('ERROR: isolate metadata file for outbreak_cluster_metadata_decoration() must be '
                                'formatted as a .csv'))
        raise

    # import isolate metadata to a Dataframe object using metadata_importer()
    isolate_metadata = metadata_importer(metadata_file=metadata_file,
                                         metadata_type='isolate',
                                         import_type='dataframe',
                                         logger=logger,
                                         debug=debug)

    decorated_cluster_dataframe = pd.merge(outbreak_cluster_dataframe, isolate_metadata, how='left', on='Sample ID')

    # format date column as datetime
    decorated_cluster_dataframe['Collection Date'] = pd.to_datetime(decorated_cluster_dataframe['Collection Date'])

    if debug:
        print(decorated_cluster_dataframe.head(20))

    return decorated_cluster_dataframe


#######################################################################################################################
#
# import_existing_outbreak_cluster_information()
#   1) validate the existing outbreak cluster information
#   2) ensure that a backup of the existing cluster information file is present, or create one
#   3) read in the existing outbreak cluster information into a dataframe
#
#######################################################################################################################
def import_existing_outbreak_cluster_information(existing_clusters_file=None, logger=None, debug=False):
    logger.log_section_header('Importing existing outbreak cluster information')
    logger.log(log.dim('\ta) Generate a time-stamped backup of the existing outbreak cluster information'))
    logger.log(log.dim('\tb) Import the existing outbreak cluster information'))
    logger.log(log.dim('\tc) Return the existing outbreak cluster information as a Dataframe object'))

    # validate passed kwargs
    try:
        assert existing_clusters_file
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg existing clusters file is required by import_existing_outbreak_cluster_'
                                'information()'))
        raise

    existing_clusters_file = path_clean(file_path=existing_clusters_file)

    try:
        assert file_check(file_path=existing_clusters_file, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red('ERROR: import_existing_outbreak_cluster_information() unable to locate the metadata_'
                                'file: ' + str(existing_clusters_file)))
        raise

    try:
        assert file_extension_check(file_path=existing_clusters_file, file_extension=['.xlsx'])
    except AssertionError:
        logger.log(log.bold_red('ERROR: existing clusters file must be a .csv file'))
        raise

    # generate a backup of the existing cluster information file
    shutil.copyfile(existing_clusters_file, existing_clusters_file.with_suffix('.xlsx.bak'))

    # import existing cluster information file into a Dataframe object
    try:
        xlsx = pd.ExcelFile(existing_clusters_file)
    except xlrd.biffh.XLRDError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(
                str(existing_clusters_file))) + 'needs to be formatted as an .xlsx')
        raise
    try:
        existing_clusters_dataframe = pd.read_excel(xlsx, header=0, dtype={'Cluster name': object})
    except xlrd.biffh.XLRDError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(
                str(existing_clusters_file))) + ' is improperly formatted!')
        raise
    except ValueError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(
                str(existing_clusters_file))) + ' is improperly formatted!')
        raise

    # format date column as datetime
    existing_clusters_dataframe['Collection Date'] = pd.to_datetime(existing_clusters_dataframe['Collection Date'])

    if debug:
        print(existing_clusters_dataframe.head(20))

    return existing_clusters_dataframe


#######################################################################################################################
#
# rename_clusters()
#   1) in the case where the historical information is not provided, rename clusters using only the current data
#
#######################################################################################################################
def rename_clusters(new_outbreak_dataframe=None, logger=None, debug=False):
    logger.log_section_header('Renaming potential outbreak clusters')
    logger.log(log.dim('\ta) Since no cluster tracking information was provided, we will rename clusters using '
                       'only data from this run'))

    # validate passed kwargs
    try:
        assert new_outbreak_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg new_outbreak_dataframe is required for '
                                'compare_new_and_old_outbreak_cluster_dataframes()'))
        raise

    # create a column (Previously reported) in the new outbreak dataframe that represents whether that isolate was
    # implicated in a previously recorded outbreak cluster. If so, (Cluster name) will be set to the corresponding
    # cluster name

    new_outbreak_dataframe['Cluster name'] = (float('NaN'))

    # cluster (re)naming. The convention we are going to be using initially will be that clusters will be named for the
    # earliest isolate in the initial detection. Isolates that are identified later as part of that cluster will share
    # the same name. In the case of convergence (two clusters are unified by the addition of an intermediate isolate),
    # all isolates of the cluster will be given the cluster name corresponding to the older of the two cluster name
    # isolates. In this case, a new column will be added which will be a list of previous cluster designations
    # associated with a given isolate

    # If there was no metadata provided, we will NOT be able to sort the clusters by 'Collection Date' as that
    # column will not exist. In that case, we will have to sort and name according to the Sample ID. Since this is
    # not a very good idea, we will also log a warning that this took place

    if 'Collection_Date' in list(new_outbreak_dataframe.columns):

        grouped_by_cluster_dataframe = new_outbreak_dataframe.groupby(['cluster'])
        cleaned_dataframe = pd.DataFrame()
        for name, group in grouped_by_cluster_dataframe:
            group = group.sort_values('Collection Date')

            # Case #1: At least one member of the cluster has a cluster name designated
            if group['Cluster name'].any():
                cluster_name = str(group['Cluster name'].loc[group['Cluster name'].first_valid_index()])
                #print(cluster_name)

            # Case #2: All isolates in the cluster are newly identified
            else:
                cluster_name = group['Sample ID'].iloc[0]

            #print(cluster_name)

            group.loc[:, 'Cluster name'] = str(cluster_name)

            #print(group)

            cleaned_dataframe = pd.concat([group, cleaned_dataframe]).sort_index()

    else:

        logger.log(log.bold_red('\n WARNING! No sample metadata was provided during this run, so the collection date of'
                                ' the isolates is unavailable. As a result sorting was done by Sample ID, so be wary of'
                                ' the cluster name!\n'))

        grouped_by_cluster_dataframe = new_outbreak_dataframe.groupby(['cluster'])
        cleaned_dataframe = pd.DataFrame()
        for name, group in grouped_by_cluster_dataframe:
            group = group.sort_values('Sample ID')

            # Case #1: At least one member of the cluster has a cluster name designated
            if group['Cluster name'].any():
                cluster_name = str(group['Cluster name'].loc[group['Cluster name'].first_valid_index()])
                #print(cluster_name)

            # Case #2: All isolates in the cluster are newly identified
            else:
                cluster_name = group['Sample ID'].iloc[0]

            #print(cluster_name)

            group.loc[:, 'Cluster name'] = str(cluster_name)

            #print(group)

            cleaned_dataframe = pd.concat([group, cleaned_dataframe]).sort_index()

    if debug:
        print(cleaned_dataframe.head(100))

    return cleaned_dataframe



#######################################################################################################################
#
# compare_new_and_old_outbreak_cluster_dataframes()
#   1) validate that both dataframes exist
#   2) compare the two dataframes, creating a new column "Previously reported" (boolean)
#   3) pull out all clusters involving new isolates and return for flash reporting
#
#######################################################################################################################
def compare_new_and_old_outbreak_cluster_dataframes(new_outbreak_dataframe=None, existing_outbreak_dataframe=None,
                                                    logger=None, debug=False):
    logger.log_section_header('Resolving isolates that have been previously implicated in potential outbreaks')
    logger.log(log.dim('\ta) Compare the new and existing outbreak cluster reports'))
    logger.log(log.dim('\tb) Create a new field in the new outbreak cluster Dataframe, "Previously reported"'))
    logger.log(log.dim('\tc) Return the outbreak cluster Dataframe with new column to indicate is an isolate being '
                       'implicated for the first time'))

    # validate passed kwargs
    try:
        assert new_outbreak_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg new_outbreak_dataframe is required for '
                                'compare_new_and_old_outbreak_cluster_dataframes()'))
        raise

    try:
        assert existing_outbreak_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg existing_outbreak_dataframe is required for '
                                'compare_new_and_old_outbreak_cluster_dataframes()'))
        raise

    # create a column (Previously reported) in the new outbreak dataframe that represents whether that isolate was
    # implicated in a previously recorded outbreak cluster. If so, (Cluster name) will be set to the corresponding
    # cluster name

    new_outbreak_dataframe['Previously reported'] = new_outbreak_dataframe["Sample ID"].isin(existing_outbreak_dataframe["Sample ID"])
    new_outbreak_dataframe = new_outbreak_dataframe.merge(existing_outbreak_dataframe[['Sample ID', 'Cluster name']], how='left', on='Sample ID')

    # cluster (re)naming. The convention we are going to be using initially will be that clusters will be named for the
    # earliest isolate in the initial detection. Isolates that are identified later as part of that cluster will share
    # the same name. In the case of convergence (two clusters are unified by the addition of an intermediate isolate),
    # all isolates of the cluster will be given the cluster name corresponding to the older of the two cluster name
    # isolates. In this case, a new column will be added which will be a list of previous cluster designations
    # associated with a given isolate

    grouped_by_cluster_dataframe = new_outbreak_dataframe.groupby(['cluster'])
    cleaned_dataframe = pd.DataFrame()
    for name, group in grouped_by_cluster_dataframe:
        group = group.sort_values('Collection Date')

        # Case #1: At least one member of the cluster has a cluster name designated
        if group['Cluster name'].any():
            cluster_name = str(group['Cluster name'].loc[group['Cluster name'].first_valid_index()])
            print(cluster_name)

        # Case #2: All isolates in the cluster are newly identified
        else:
            cluster_name = group['Sample ID'].iloc[0]

        print(cluster_name)

        group.loc[:, 'Cluster name'] = str(cluster_name)

        print(group)

        cleaned_dataframe = pd.concat([group, cleaned_dataframe]).sort_index()


    if debug:
        print(cleaned_dataframe.head(100))

    return cleaned_dataframe


#######################################################################################################################
#
# filter_same_patient_clusters()
#   1) Get a count of the number of the number of a) isolates and b) UNIQUE patients for each potential outbreak cluster
#   2) Return a dataframe that removes any potential outbreak clusters that only involve a single patient
#
#######################################################################################################################
def filter_same_patient_clusters(outbreak_cluster_dataframe=None, args=None, logger=None, debug=False):

    # validate passed kwargs
    try:
        assert outbreak_cluster_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg outbreak_cluster_dataframe is required for '
                                'filter_same_patient_clusters()'))
        raise

    # Get a count of the number of the number of a) isolates and b) UNIQUE patients for each potential outbreak cluster

    filtered_dataframe = pd.DataFrame()

    # we will do same-source deduplication one of two ways based on whether or not cluster-tracking was provided: a)
    # if it was we will use cluster name as the key or b) if it was not we will use the raw cluster value

    if args.cluster_tracking:
        group_by_cluster_dataframe = outbreak_cluster_dataframe.groupby(['Cluster name'])
        for name, group in group_by_cluster_dataframe:
            print(('\ncluster name: {}').format(name))
            print(('number of isolates in group: {}').format(len(group.index)))
            print(('number of unique patients in group: {}').format(group['Patient ID'].nunique()))

            if group['Patient ID'].nunique() < 2:
                logger.log(log.yellow(('\nCluster {} contained {} isolates but all shared the same patient ID, so this '
                                       'cluster is being removed\n').format(name, len(group.index))))
                continue

            filtered_dataframe = pd.concat([group, filtered_dataframe]).sort_index()

    else:
        group_by_cluster_dataframe = outbreak_cluster_dataframe.groupby(['cluster'])
        for name, group in group_by_cluster_dataframe:
            print(('\ncluster name: {}').format(name))
            print(('number of isolates in group: {}').format(len(group.index)))
            print(('number of unique patients in group: {}').format(group['Patient ID'].nunique()))

            if group['Patient ID'].nunique() < 2:
                logger.log(log.yellow(
                    ('\nCluster {} contained {} isolates but all shared the same patient ID, so this cluster is being '
                     'removed\n').format(name, len(group.index))))
                continue

            filtered_dataframe = pd.concat([group, filtered_dataframe]).sort_index()

    if debug:
        print(filtered_dataframe)

    return filtered_dataframe


#######################################################################################################################
#
#
#
#
#######################################################################################################################



#######################################################################################################################
#
# generate_flash_report()
#   1) validate that the flash report dataframe exists
#   2) group by facility, and write out report for that facility
#
#######################################################################################################################
def generate_flash_report(input_dataframe=None, output_directory=None, logger=None, debug=False):

    # validate passed kwargs
    try:
        assert input_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg input_dataframe is required for generate_flash_report()'))
        raise

    try:
        assert Path(output_directory).expanduser().resolve().is_dir()
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg output_directory is required for generate_flash_report()'))
        raise

    # TODO figure out how I want to structure the report generation workflow. Is it better to split the outbreak cluster
    #  matrix into a) facility specific matrices or b) facility and cluster specific matrices PRIOR to the
    #  generate_flash_report() method or is it simpler to just perform that action here?

    # TODO figure out the essential reportable fields from the outbreak cluster dataframe and how best to present them




#######################################################################################################################
#
# generate_MRsnp_input_file()
#   1) validate that the flash report dataframe exists
#   2) write out MRsnp input file
#
#######################################################################################################################


#######################################################################################################################
#
# metadata_importer():
#   Import the metadata information into a pandas dataframe or dictionary
#
#######################################################################################################################
def metadata_importer(metadata_file=None, metadata_type=None, import_type=None, logger=None, debug=False):
    try:
        assert metadata_file
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg metadata_file is required'))
        raise
    try:
        assert metadata_type in ['isolate', 'amr', 'cluster']
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg metadata_type is required and must be either "isolate" or "amr"'))
        raise
    try:
        assert import_type in ['dataframe', 'dictionary']
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg import_type is required and must be either "dataframe" or "dictionary"'))
        raise

    metadata_file = path_clean(file_path=metadata_file)

    try:
        assert file_check(file_path=metadata_file, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red('ERROR: metadata_importer() unable to locate the metadata_file: ' + str(metadata_file)))
        raise

    try:
        assert file_extension_check(file_path=metadata_file, file_extension=['.csv'])
    except AssertionError:
        logger.log(log.bold_red('ERROR: metadata_file must be a .csv file'))
        raise

    if import_type == 'dataframe':

        if metadata_type == 'isolate':
            column_list = ['Sample ID', 'ST', 'Project Code', 'Shipment', 'Patient ID', 'CHCS #',
                           'Collection Date', 'Isolation Source', 'Organism ID', 'Sequencing ID']
        elif metadata_type == 'amr':
            column_list = ['Allele', 'Significance Score', 'Gene Family', 'Predicted Phenotype']

        try:
            metadata = pd.read_csv(metadata_file, sep=',', usecols=column_list)
        except pd.errors.EmptyDataError:  # Case of empty csv
            logger.log('The following metadata file was empty: ' + log.bold(str(metadata_file)))
            return
        except ValueError:  # Incorrectly formatted csv
            logger.log('The following metadata file was incorrectly formatted: ' + log.bold(str(metadata_file)))
            return

        if debug:
            print(metadata)

        return metadata

    elif import_type == 'dictionary':
        metadata = {}
        with open(metadata_file, 'r') as infile:
            reader = csv.reader(infile)
            for row in reader:
                key = row[0]
                value = [item for item in row[1:]]
                metadata[key] = value

        if debug:
            for key, value in metadata.items():
                logger.log('Key: ' + str(key) + ' contains the following values')
                for isolate in value:
                    logger.log(str(isolate))

        return metadata
