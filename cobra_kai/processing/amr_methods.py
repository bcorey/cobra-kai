#!/usr/bin/env python
# encoding: utf-8
import sys

import pandas as pd
import shutil
from pathlib import Path

import xlrd

from cobra_kai.common import log
from cobra_kai.common.miscellaneous import file_check, path_clean, file_extension_check


#######################################################################################################################
#
# import_amr_data():
#   Import the curated AMR output from MIGHT into a dataframe object
#
#######################################################################################################################
def import_amr_data(amr_summary_csv=None, logger=None, debug=False):
    logger.log_section_header('Reading in data from the amr_summary.csv file')

    try:
        assert amr_summary_csv
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg amr_summary_csv is required'))
        raise

    amr_summary_csv = path_clean(file_path=amr_summary_csv)

    try:
        assert file_check(file_path=amr_summary_csv, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red('ERROR: import_amr_data() unable to locate the file: ' + str(amr_summary_csv)))
        raise

    try:
        assert file_extension_check(file_path=amr_summary_csv, file_extension=['.csv'])
    except AssertionError:
        logger.log(log.bold_red('ERROR: amr_summary_csv must be a .csv file'))
        raise

    column_list = ['Sample', 'Source', 'Gene symbol', 'Sequence name', 'Method',
                   '% Coverage of reference sequence', '% Identity to reference sequence', 'Start',
                   'Stop', 'Strand', 'Contig id', 'Contig length', 'Accession of closest sequence',
                   'Name of closest sequence', 'Scope', 'Element type', 'Element subtype', 'Class',
                   'Subclass']

    try:
        amr_summary_dataframe = pd.read_csv(amr_summary_csv, sep=',', usecols=column_list)
    except pd.errors.EmptyDataError:  # Case of empty amr_summary_csv
        logger.log('The AMR report file was empty: ' + log.bold(str(amr_summary_csv)))
        return
    except ValueError:  # Incorrectly formatted amr_summary_csv
        logger.log('The AMR report file was incorrectly formatted: ' + log.bold(str(amr_summary_csv)))
        return

    if debug:
        print(amr_summary_dataframe)

    return amr_summary_dataframe


#######################################################################################################################
#
# filter_insignificant_genes():
#   perform specified filter actions on the AMR dataframe
#   returns an amr Dataframe
#
#######################################################################################################################
def filter_insignificant_genes(output_directory=None, amr_dataframe=None, filter_file=None, logger=None, debug=False):
    logger.log_section_header('Filtering low significance genes from input')

    try:
        assert amr_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg amr_dataframe is required'))
        raise

    try:
        assert filter_file is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg filter_file is required'))
        raise

    try:
        assert file_check(file_path=filter_file, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red(
            'ERROR: cannot locate ' + log.green('filter file') + ' at ' + log.bold_red(str(filter_file))))
        raise

    # TODO clean up error handling of non-xlsx formatted files
    # Attempt to open the filter_file using pandas
    try:
        xlsx = pd.ExcelFile(filter_file)
    except xlrd.biffh.XLRDError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(str(filter_file))) + 'needs to be formatted as a .xlsx')
        raise

    # Attempt to read in the Gene Significance tab using pandas
    try:
        filter_dataframe = pd.read_excel(xlsx, 'Gene Significance', index_col=0, header=0)
    except xlrd.biffh.XLRDError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(
                str(filter_file))) + ' requires a tab named "Gene Significance" for filtering!')
        raise
    except ValueError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(str(filter_file))) + ' requires a tab named "Gene Significance" for filtering!')
        raise

    if debug:
        print(filter_dataframe)

    # merge the amr_dataframe with the filter_dataframe on Gene symbol to add Gene Significance data
    filterable_dataframe = pd.merge(amr_dataframe, filter_dataframe, how='left', on='Gene symbol')

    if debug:
        print(filterable_dataframe)

    # get all of the samples for which amr_data is present
    samples = sorted(list(set(amr_dataframe['Sample'].tolist())))

    if debug:
        print(samples)

    # filter out rows where Gene Significance != high
    filtered_dataframe = filterable_dataframe.mask(filterable_dataframe['Gene Significance'] != 'high').dropna()

    # write the filtered dataframe to output file
    filtered_dataframe_file = output_directory / 'filtered_AMR_report.xlsx'

    filtered_dataframe.to_excel(filtered_dataframe_file, sheet_name='filtered_AMR_data', index=
    False)

    try:
        assert file_check(file_path=filtered_dataframe_file, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red('ERROR: cannot locate ' + log.green('filtered output file') + ' at ' + log.bold_red(str(filtered_dataframe_file))))
        raise

    logger.log('The filtered AMR output file is: ' + log.green(str(filtered_dataframe_file)))

    # add back a placeholder row for all samples that have been otherwise completely filtered out
    for sample in samples:
        filtered_dataframe = filtered_dataframe.append({'Sample': sample}, ignore_index=True)

    if debug:
        print(filtered_dataframe)

    return filtered_dataframe


#######################################################################################################################
#
# color_code_genes():
#   perform specified filter actions on the AMR dataframe
#   returns an amr Dataframe
#
#######################################################################################################################
def color_code_genes(output_directory=None, amr_dataframe=None, filter_file=None, logger=None, debug=False):
    logger.log_section_header('Filtering low significance genes from input')

    try:
        assert amr_dataframe is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg amr_dataframe is required'))
        raise

    try:
        assert filter_file is not None
    except AssertionError:
        logger.log(log.bold_red('ERROR: kwarg filter_file is required'))
        raise

    try:
        assert file_check(file_path=filter_file, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red(
            'ERROR: cannot locate ' + log.green('filter file') + ' at ' + log.bold_red(str(filter_file))))
        raise

    # TODO clean up error handling of non-xlsx formatted files
    # Attempt to open the filter_file using pandas
    try:
        xlsx = pd.ExcelFile(filter_file)
    except xlrd.biffh.XLRDError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(str(filter_file))) + 'needs to be formatted as a .xlsx')
        raise

    # Attempt to read in the Gene Significance tab using pandas
    try:
        filter_dataframe = pd.read_excel(xlsx, 'Gene Significance', index_col=0, header=0)
    except xlrd.biffh.XLRDError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(
                str(filter_file))) + ' requires a tab named "Gene Significance" for filtering!')
        raise
    except ValueError:
        logger.log(log.bold_red(
            'ERROR: ' + log.green('filter file') + ' at ' + log.bold_red(str(filter_file))) + ' requires a tab named "Gene Significance" for filtering!')
        raise

    if debug:
        print(filter_dataframe)

    # merge the amr_dataframe with the filter_dataframe on Gene symbol to add Gene Significance data
    color_coded_dataframe = pd.merge(amr_dataframe, filter_dataframe, how='left', on='Gene symbol')

    color_coded_dataframe['iTOL color code'] = color_coded_dataframe['iTOL color code'].fillna('rgb(171, 209, 224)')

    return color_coded_dataframe


#######################################################################################################################
#
# amr_itol_dataset_writer():
#   write the AMR information from the amr_dataframe to an iTOL-formatted dataset
#
#   Color coding may be applied according to gene family OR predicted resistance phenotype
#
#######################################################################################################################
def amr_itol_dataset_writer(output_directory=None, amr_dataframe=None, logger=None, debug=False):
    logger.log_section_header('Validating template files are available')

    # verify that the two required template files are in place
    top_template = Path(__file__).resolve().parent.parent / 'resources' / 'itol_AMR_template_1.txt'
    bottom_template = Path(__file__).resolve().parent.parent / 'resources' / 'itol_AMR_template_2.txt'

    try:
        assert file_check(file_path=top_template, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red('ERROR: cannot locate ' + log.green('top template file') + ' at ' + log.bold_red(str(top_template))))
        raise

    logger.log('\nThe top template file is: ' + log.green(str(top_template)))

    try:
        assert file_check(file_path=bottom_template, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red(
            'ERROR: cannot locate ' + log.green('bottom template file') + ' at ' + log.bold_red(str(bottom_template))))
        raise

    logger.log('The bottom template file is: ' + log.green(str(bottom_template)))

    output_file = output_directory / 'amr_itol_dataset.txt'

    # copy top template to the output file
    shutil.copyfile(top_template, output_file)

    try:
        assert file_check(file_path=output_file, file_or_directory='file')
    except AssertionError:
        logger.log(log.bold_red('ERROR: cannot locate ' + log.green('output file') + ' at ' + log.bold_red(str(output_file))))
        raise

    logger.log('The amr iTOL output file is: ' + log.green(str(output_file)))

    # get all of the samples for which amr_data is present
    samples = sorted(list(set(amr_dataframe['Sample'].tolist())))

    if debug:
        print(samples)

    # drop NAN gene calls
    #amr_dataframe = amr_dataframe.dropna()

    # collect a list of all alleles in the dataframe
    amr_alleles = sorted(list(set(amr_dataframe['Gene symbol'].tolist())))

    if debug:
        print(amr_alleles)

    # prepare all of the output to be written to the file
    field_shapes_line = 'FIELD_SHAPES\t' + '2\t' * (len(amr_alleles) - 1) + '2\n\n'
    field_labels_line = 'FIELD_LABELS\t' + '\t'.join(amr_alleles) + '\n'

    # if dataframe contains a column named iTOL color code, use this for field colors, else use default

    if 'iTOL color code' in amr_dataframe.columns:

        color_codes = []

        for allele in amr_alleles:
            color_code = (amr_dataframe['iTOL color code'][amr_dataframe['Gene symbol'] == allele]).iloc[0]
            print(allele, color_code)
            color_codes.append(color_code)

        field_colors_line = 'FIELD_COLORS\t' + '\t'.join(color_codes) + '\n'

    else:
        field_colors_line = None

    if debug:
        print(field_shapes_line)
        print(field_labels_line)
        print(field_colors_line)

    # generate presence/absence data based on Sample/Gene symbol combinations
    sample_presence_absence = []
    for sample in samples:
        if str(sample).startswith('MRSN'):
            sample_line = [str(sample)[4:]]
        else:
            sample_line = [str(sample)]
        for allele in amr_alleles:
            if debug:
                print(sample)
                print(allele)
                print(((amr_dataframe['Sample'] == sample) & (amr_dataframe['Gene symbol'] == allele)).any())
            if ((amr_dataframe['Sample'] == sample) & (amr_dataframe['Gene symbol'] == allele)).any():
                sample_line.append('1')
            else:
                sample_line.append('0')
        sample_presence_absence.append(sample_line)

    if debug:
        print(sample_presence_absence)

    # display a preview of the results table
    logger.log_section_header('Data preview')
    logger.log_explanation('The following is a preview of the data that will be written to the output file')
    logger.log(field_labels_line)
    for line in sample_presence_absence:
        logger.log('\t'.join(line))

    logger.log_section_header('Writing output to amr_summary_for_iTOL.txt')

    # add the FIELD SHAPES and FIELD LABELS information
    with open(output_file, 'a') as out_file:

        out_file.write(field_shapes_line)
        out_file.write(field_labels_line)

        if field_colors_line:
            out_file.write(field_colors_line)

        # write in the content of the bottom template
        with open(bottom_template, 'r') as bt:
            for line in bt.readlines():
                out_file.write(line)

        # write in the DATA information
        for sample_line in sample_presence_absence:
            out_file.write('\t'.join(sample_line) + '\n')

    logger.log(log.green('complete!'))