#!/usr/bin/env python
# encoding: utf-8

"""
Runs methods under several conditions and looks for failure points
"""
import sys
from pathlib import Path
import cobra_kai.testing as testing


def test_main(args):
    if args.command == 'amr':
        test_amr_workflow()

    if args.command == 'cluster':
        test_cluster_workflow()

    if args.command == 'color-coding':
        test_amr_color_coding()


#######################################################################################################################
#
# tests for amr_itol_dataset_writer()
#
#
#######################################################################################################################
def test_amr_workflow():
    import subprocess
    from cobra_kai.common import log
    from cobra_kai.processing.amr_methods import import_amr_data, amr_itol_dataset_writer

    breakline = '#################################################################################################\n'

    testing_log_dir = Path(testing.__file__).parent / 'test_logs'
    if not testing_log_dir.is_dir():
        subprocess.run(['mkdir', str(testing_log_dir)])
    testing_log_file = testing_log_dir / 'test_log.txt'
    logger = log.Log(log_filename=testing_log_file)

    test_output_directory = logger.log_file_path.parent
    test_amr_data = Path(testing.__file__).parent / 'test_resources' / 'amr_testing' / 'test_MIGHT_output.csv'

    logger.log('Starting test of method: ' + log.bold_red('import_amr_data()'))
    amr_dataframe = import_amr_data(amr_summary_csv=test_amr_data, logger=logger, debug=True)
    logger.log(log.bold_red('\nimport_amr_data()') + ' finished without errors\n')

    logger.log('\n' + breakline)

    logger.log('Starting test of method: ' + log.bold_red('amr_itol_dataset_writer()'))
    amr_itol_dataset_writer(output_directory=test_output_directory, amr_dataframe=amr_dataframe, logger=logger,
                            debug=True)
    logger.log(log.bold_red('\namr_itol_dataset_writer()') + ' finished without errors\n')

    #subprocess.run('clear')

    logger.log(log.bold_green('\namr workflow tests finished without errors\n'))


#######################################################################################################################
#
# tests for amr_filtering
#
#
#######################################################################################################################
def test_amr_filtering():
    import subprocess
    from cobra_kai.common import log
    from cobra_kai.processing.amr_methods import import_amr_data, amr_itol_dataset_writer
    from cobra_kai.processing.amr_methods import filter_insignificant_genes

    breakline = '#################################################################################################\n'

    testing_log_dir = Path(testing.__file__).parent / 'test_logs'
    if not testing_log_dir.is_dir():
        subprocess.run(['mkdir', str(testing_log_dir)])
    testing_log_file = testing_log_dir / 'test_log.txt'
    logger = log.Log(log_filename=testing_log_file)

    test_output_directory = logger.log_file_path.parent
    test_amr_data = Path(testing.__file__).parent / 'test_resources' / 'GS01.5_ACB_AMR_curated.csv'

    logger.log('Starting test of method: ' + log.bold_red('import_amr_data()'))
    amr_dataframe = import_amr_data(amr_summary_csv=test_amr_data, logger=logger, debug=True)
    logger.log(log.bold_red('\nimport_amr_data()') + ' finished without errors\n')

    logger.log('\n' + breakline)

    # Begin filtering method(s) tests(s)

    significance_filter_file = Path(testing.__file__).parent / 'test_resources' / 'significance_filter_test_3.xlsx'

    logger.log('Starting test of method: ' + log.bold_red('filter_insignificant_genes()'))
    significance_filtered_amr_dataframe = filter_insignificant_genes(output_directory=test_output_directory,
                                                                     amr_dataframe=amr_dataframe,
                                                                     filter_file=significance_filter_file,
                                                                     logger=logger, debug=True)
    logger.log(log.bold_red('\nimport_amr_data()') + ' finished without errors\n')

    logger.log('\n' + breakline)

    logger.log('Starting test of method: ' + log.bold_red('amr_itol_dataset_writer()'))
    amr_itol_dataset_writer(output_directory=test_output_directory, amr_dataframe=amr_dataframe, logger=logger,
                            debug=True)
    logger.log(log.bold_red('\namr_itol_dataset_writer()') + ' finished without errors\n')

    # subprocess.run('clear')

    logger.log(log.bold_green('\namr workflow tests finished without errors\n'))


#######################################################################################################################
#
# tests for amr_color_coding
#
#
#######################################################################################################################
def test_amr_color_coding():
    import subprocess
    import sys
    from cobra_kai.common import log
    from cobra_kai.processing.amr_methods import import_amr_data, amr_itol_dataset_writer
    from cobra_kai.processing.amr_methods import color_code_genes

    breakline = '#################################################################################################\n'

    testing_log_dir = Path(testing.__file__).parent / 'test_logs'
    if not testing_log_dir.is_dir():
        subprocess.run(['mkdir', str(testing_log_dir)])
    testing_log_file = testing_log_dir / 'test_log.txt'
    logger = log.Log(log_filename=testing_log_file)

    test_output_directory = logger.log_file_path.parent
    test_amr_data = Path(testing.__file__).parent / 'test_resources' / 'amr_testing' / 'test_MIGHT_output.csv'

    logger.log('Starting test of method: ' + log.bold_red('import_amr_data()'))
    amr_dataframe = import_amr_data(amr_summary_csv=test_amr_data, logger=logger, debug=True)
    logger.log(log.bold_red('\nimport_amr_data()') + ' finished without errors\n')

    logger.log('\n' + breakline)

    # Begin filtering method(s) tests(s)

    significance_filter_file = Path(testing.__file__).parent / 'test_resources' / 'amr_testing' / 'significance_filter_test_3.xlsx'

    logger.log('Starting test of method: ' + log.bold_red('color_code_genes()'))
    color_coded_amr_dataframe = color_code_genes(output_directory=test_output_directory, amr_dataframe=amr_dataframe,
                                                 filter_file=significance_filter_file, logger=logger, debug=True)
    logger.log(log.bold_red('\ncolor_code_genes()') + ' test finished without errors\n')

    print(color_coded_amr_dataframe)

    print(set(color_coded_amr_dataframe['iTOL color code'].values))

    logger.log('\n' + breakline)

    logger.log('Starting test of method: ' + log.bold_red('amr_itol_dataset_writer()'))
    amr_itol_dataset_writer(output_directory=test_output_directory, amr_dataframe=color_coded_amr_dataframe,
                            logger=logger, debug=True)
    logger.log(log.bold_red('\namr_itol_dataset_writer()') + ' finished without errors\n')

    # subprocess.run('clear')

    logger.log(log.bold_green('\namr workflow tests finished without errors\n'))


#######################################################################################################################
#
# tests for cluster workflow
#
#
#######################################################################################################################
def test_cluster_workflow():
    import subprocess
    import pandas as pd
    from cobra_kai.common import log
    from cobra_kai.processing.clustering_methods import distance_matrix_to_dataframe, \
        dataframe_to_pairwise_distance, threshold_filter, multi_index_series_to_dict, \
        generate_potential_transmission_itol_dataset, outbreak_cluster_detection, outbreak_cluster_metadata_decoration, \
        import_existing_outbreak_cluster_information, compare_new_and_old_outbreak_cluster_dataframes, \
        filter_same_patient_clusters

    testing_log_dir = Path(testing.__file__).parent / 'test_logs'
    if not testing_log_dir.is_dir():
        subprocess.run(['mkdir', str(testing_log_dir)])
    testing_log_file = testing_log_dir / 'test_log.txt'
    logger = log.Log(log_filename=testing_log_file)

    breakline = '#################################################################################################\n'

    distance_matrix_data_new = Path(
        testing.__file__).parent / 'test_resources' / 'cluster_testing' / 'sorted_distance_matrix_file_new.xlsx'
    distance_matrix_data_old = Path(
        testing.__file__).parent / 'test_resources' / 'cluster_testing' / 'sorted_distance_matrix_file_old.xlsx'

    logger.log('\n' + breakline)

    logger.log('Starting test of method: ' + log.bold_red('distance_matrix_to_dataframe()'))
    distance_matrix_dataframe = distance_matrix_to_dataframe(distance_matrix_file=distance_matrix_data_new,
                                                             logger=logger, debug=True)
    logger.log(log.bold_red('\ndistance_matrix_to_dataframe()') + ' finished without errors\n')

    logger.log(breakline)

    logger.log('Starting test of method: ' + log.bold_red('dataframe_to_pairwise_distance()'))
    pairwise_series = dataframe_to_pairwise_distance(distance_dataframe=distance_matrix_dataframe, logger=logger,
                                                     debug=True)
    logger.log(log.bold_red('\ndataframe_to_pairwise_distance()') + ' finished without errors\n')

    logger.log(breakline)

    logger.log('Starting test of method: ' + log.bold_red('threshold_filter()') + ' with a threshold of 15')
    filtered_pairwise_series = threshold_filter(series_object=pairwise_series, threshold=15, logger=logger, debug=True)
    logger.log(log.bold_red('\nthreshold_filter()') + ' finished without errors\n')

    logger.log(breakline)

    logger.log('Starting test of method: ' + log.bold_red('multi_index_series_to_dict()'))
    multi_index_dict = multi_index_series_to_dict(series_object=filtered_pairwise_series, logger=logger, debug=True)
    logger.log(log.bold_red('\nmulti_index_series_to_dict()') + ' finished without errors\n')

    logger.log(breakline)

    test_output_directory = logger.log_file_path.parent

    logger.log('Starting test of method: ' + log.bold_red('generate_potential_transmission_itol_dataset()'))
    generate_potential_transmission_itol_dataset(potential_transmission_dict=multi_index_dict,
                                                 output_directory=test_output_directory, logger=logger, debug=True)
    logger.log(log.bold_red('\ngenerate_potential_transmission_itol_dataset()') + ' finished without errors\n')

    logger.log(breakline)

    logger.log('Starting test of method: ' + log.bold_red('outbreak_cluster_detection()') + ' with an eps value of 15')
    cluster_dataframe = outbreak_cluster_detection(distance_matrix_dataframe=distance_matrix_dataframe, eps=15,
                                                   logger=logger, debug=True)
    logger.log(log.bold_red('\noutbreak_cluster_detection()') + ' finished without errors\n')

    logger.log(breakline)

    metadata_file = Path(testing.__file__).parent / 'test_resources' / 'cluster_testing' / 'test_metadata_file.csv'

    logger.log('Starting test of method: ' + log.bold_red('outbreak_cluster_metadata_decoration()'))
    decorated_cluster_dataframe = outbreak_cluster_metadata_decoration(outbreak_cluster_dataframe=cluster_dataframe,
                                                                       metadata_file=metadata_file, logger=logger,
                                                                       debug=True)
    logger.log(log.bold_red('\noutbreak_cluster_metadata_decoration()') + ' finished without errors\n')

    logger.log(breakline)

    existing_clusters_file = Path(
        testing.__file__).parent / 'test_resources' / 'cluster_testing' / 'cluster_analysis_old.xlsx'

    logger.log('Starting test of method: ' + log.bold_red('import_existing_outbreak_cluster_information()'))
    existing_cluster_dataframe = import_existing_outbreak_cluster_information(
        existing_clusters_file=existing_clusters_file, logger=logger, debug=True)
    logger.log(log.bold_red('\nimport_existing_outbreak_cluster_information()') + ' finished without errors\n')

    logger.log(breakline)

    logger.log('Starting test of method: ' + log.bold_red('compare_new_and_old_outbreak_cluster_dataframes()'))
    cluster_dataframe = compare_new_and_old_outbreak_cluster_dataframes(
        new_outbreak_dataframe=decorated_cluster_dataframe,
        existing_outbreak_dataframe=existing_cluster_dataframe,
        logger=logger, debug=True)
    logger.log(log.bold_red('\ncompare_new_and_old_outbreak_cluster_dataframes()') + ' finished without errors\n')

    logger.log(breakline)

    class args():
        def __init__(self, existing_clusters_file):
            self.cluster_tracking = existing_clusters_file

    args = args(existing_clusters_file)

    logger.log('Starting test of method: ' + log.bold_red('filter_same_patient_clusters()'))
    filtered_cluster_dataframe = filter_same_patient_clusters(outbreak_cluster_dataframe=cluster_dataframe,
                                                              args=args,
                                                              logger=logger, debug=True)
    logger.log(log.bold_red('\nfilter_same_patient_clusters()') + ' finished without errors\n')

    logger.log(breakline)


    new_cluster_file = Path(
       testing.__file__).parent / 'test_logs' / 'cluster_analysis_new.xlsx'

    with pd.ExcelWriter(new_cluster_file, mode='w') as writer:
        cluster_dataframe.to_excel(writer, sheet_name='unfiltered', index=False)
        filtered_cluster_dataframe.to_excel(writer, sheet_name='filtered', index=False)

    logger.log(log.bold_green('\ncluster workflow tests finished without errors\n'))

